// g++ -static -static-libgcc -static-libstdc++ main.cpp -o comp_alg -Wall -Wextra -std=c++11 -O3

#define BENCHMARK

#include <iostream>
#include <vector>
#include <fstream>
#include <iterator>
#include <algorithm>
#include <chrono>

#ifdef BENCHMARK
	#define CHECK_FILESTREAM(f, n) if (!f.good()) { std::cout << "File \"" << n << "\" could not be read properly." << "\n"; f.close();  std::exit(EXIT_FAILURE); }
	#define START_BENCH() start = std::chrono::system_clock::now();
	#define END_BENCH(m) end = std::chrono::system_clock::now(); diff = end - start; std::cout << m << diff.count() << "(s)\n";
#elif
	#define CHECK_FILESTREAM(f, n)
	#define START_BENCH()
	#define END_BENCH(m)
#endif

struct Result {
	size_t line_n_a;
	size_t line_n_b;
	unsigned int result;
#ifdef FULL_STR
	std::string a;
	std::string b;
#endif
};

std::vector<std::string> file_a_contents;
std::vector<std::string> file_b_contents;
std::vector<Result> results;

#ifdef BENCHMARK
	std::chrono::time_point<std::chrono::system_clock> start, end;
	std::chrono::duration<double> diff;
#endif

unsigned int LevenshteinDistance(const std::string& s1, const std::string& s2) 
{
	const std::size_t len1 = s1.size(), len2 = s2.size();
	std::vector<unsigned int> col(len2 + 1), prev_col(len2 + 1);
	
	for (unsigned int i = 0; i < prev_col.size(); i++) {
		prev_col[i] = i;
	}
	for (unsigned int i = 0; i < len1; i++) {
		col[0] = i + 1;
		for (unsigned int j = 0; j < len2; j++)
			col[j + 1] = std::min(std::min(prev_col[1 + j] + 1, col[j] + 1), prev_col[j] + (s1[i] == s2[j] ? 0 : 1));
		col.swap(prev_col);
	}
	return prev_col[len2];
}

std::string RandomStr(const int len) {
	std::string s = "";

    static const char alphanum[] =
        "ABCDEFGHIJKLMNOPQRSTUVWXYZ"
        "abcdefghijklmnopqrstuvwxyz";

    for (int i = 0; i < len; ++i) {
        s.push_back(alphanum[rand() % (sizeof(alphanum) - 1)]);
    }

    s[len] = 0;

	return s;
}

void CreateBenchFiles(int num_lines, int line_length, std::string file_name) {
	std::ofstream file;
	file.open(file_name, std::ofstream::out | std::ofstream::trunc);

	std::string contents = "";

	for (auto i = 0; i < num_lines; i++) {
		contents += RandomStr(line_length) + "\n";
	}

	file << contents;
	file.close();
}

void ReadAndCopyToStack(char* argv[]) {
	using istream_str_iterator = std::istream_iterator<std::string>;
	std::ifstream file_a;
	std::ifstream file_b;

	file_a.open(argv[1], std::ifstream::in);
	CHECK_FILESTREAM(file_a, argv[1])

	file_b.open(argv[2], std::ifstream::in);
	CHECK_FILESTREAM(file_b, argv[2])

	std::copy(istream_str_iterator(file_a), istream_str_iterator(), std::back_inserter(file_a_contents)); 
	file_a.close();

	std::copy(istream_str_iterator(file_b), istream_str_iterator(), std::back_inserter(file_b_contents));
	file_b.close();
}

void OutputResults(int max_dist) {
	std::cout << "=== RESULTS ===\n";

	for (auto r : results) {
		if (max_dist > 0) {
			if (r.result > max_dist) return;
		}

		std::cout << "A: " << r.line_n_a << " B: " << r.line_n_b << " Result: " << r.result << "\n";
	}
}

bool HasArgument(std::string str, int argc, char *argv[], int& index) {
	for (auto i = 0; i < argc; i++) {
		if (argv[i] == str) {
			index = i;
			return true;
		}
	}

	return false;
}

int main(int argc, char *argv[]) {
	if (argc < 2) {
		std::cout << "Detected a incorrect amount of arguments..." << "\n";
		std::cout << "Usage: ./alg.exe [file1] [file2]" << "\n";
		std::cout << "Or generate benchmark files using: ./alg.exe -gen" << "\n";
		std::exit(EXIT_FAILURE);
	}

	int gen_arg_idx;
	if (HasArgument("-gen", argc, argv, gen_arg_idx)) {
		START_BENCH()
		srand (time(NULL));

		unsigned int num_lines = 500;
		unsigned int line_length = 20;
		if (argc > gen_arg_idx + 1) {
			num_lines = atoi(argv[gen_arg_idx + 1]);
		}
		if (argc > gen_arg_idx + 2) {
			line_length = atoi(argv[gen_arg_idx + 2]);
		}

		CreateBenchFiles(num_lines, line_length, "b0.txt");
		CreateBenchFiles(num_lines, line_length, "b1.txt");
		END_BENCH("Benchmark file generation took: ")
		std::exit(0);
	}

	if (argc < 3) {
		std::cout << "Detected a incorrect amount of arguments..." << "\n";
		std::cout << "Usage: ./alg.exe [file1] [file2]" << "\n";
		std::exit(EXIT_FAILURE);
	}

	// Read Input
	START_BENCH()
	ReadAndCopyToStack(argv);
	END_BENCH("Reading took: ")

	// Comparison
	START_BENCH()
	for (size_t i = 0; i < file_a_contents.size(); i++) {
		for (size_t j = 0; j < file_b_contents.size(); j++) {
			unsigned int r = LevenshteinDistance(file_a_contents[i], file_b_contents[j]);
#ifdef FULL_STR
			results.push_back( {i, j, r, file_a_contents[i], file_b_contents[j]} );
#endif
			results.push_back( {i, j, r} );
		}
	}
	END_BENCH("Levenshtein comparisons took: ")

	// Sort Results
	START_BENCH()
	std::sort(results.begin(), results.end(), [](Result a, Result b){
		return (a.result < b.result);
	});
	END_BENCH("Sorting took: ")

	OutputResults(3);

	return 0;
}
