# Levenshtein file compare tool.

Simple levenshtein program to compare files line for line. (\n)

## Usage

Compare two files:

```zsh
./alg.exe [file1] [file2]
```

Generate benchmark files:

```zsh
./alg.exe -gen
```

To change the number of benchmark lines or the line length recompiling is necessary


## Compiling

> GCC
```zsh
g++ main.cpp -o comp_alg -Wall -Wextra -std=c++11 -O3
```
